//
//  JobsViewController.swift
//  BrokenCV
//
//  Created by DevComputer on 2017-10-28.
//  Copyright © 2017 Timo Brehmer. All rights reserved.
//

import UIKit

class JobsViewController: UIViewController {

    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var jobTitle: UILabel!
    @IBOutlet weak var jobDesc: UILabel!
    
    var jobs : [Job] = []
    var jobNumber = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Define jobs
        defineJobs()
        
        //Dynamically load images and text depending on choice
        let actualImage = UIImage(named: jobs[(jobNumber - 1)].image)!
        jobImage.image = actualImage
        jobTitle.text = jobs[(jobNumber - 1)].title
        jobDesc.text = jobs[(jobNumber - 1)].description
        
        //Make sure the description begins from the top left
        jobDesc.sizeToFit()

        // Do any additional setup after loading the view.
    }
    
    func defineJobs() {
        let job1 : Job = Job(image: "job1", title: "JU Education", description: "Student - Studying for a degree in civil engineering, programming.")
        let job2 : Job = Job(image: "job2", title: "Jmini", description: "Programmer - Handled framework migration from ASP.NET to WordPress. Also partly handled functionality implementations of the new framework.")
        let job3 : Job = Job(image: "job3", title: "Kumlabybadet", description: "Pool assistant - Helped out with keeping track of the outdoor swimming pool, gardening work as well as cleaning.")
        jobs = [job1, job2, job3]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
