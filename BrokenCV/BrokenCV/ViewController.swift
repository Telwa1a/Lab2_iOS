//
//  ViewController.swift
//  BrokenCV
//
//  Created by Timo Brehmer on 2017-10-19.
//  Copyright © 2017 Timo Brehmer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Link to webpage for Broken's CV
        let brokenUrl = URL(string: "https://brokenideascompany.newgrounds.com/")
        
        webView.loadRequest(URLRequest(url: brokenUrl!))
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

