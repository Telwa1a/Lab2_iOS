//
//  AddressViewController.swift
//  BrokenCV
//
//  Created by DevComputer on 2017-10-28.
//  Copyright © 2017 Timo Brehmer. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddressViewController: UIViewController {

    @IBOutlet weak var mapViewus: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configure coordinates
        let latitudus = 58.02474
        let longitudus = 14.46796
        
        //Define coordinates, span and region
        let coordinatus : CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitudus, longitudus)
        let spanus: MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let regionus : MKCoordinateRegion = MKCoordinateRegionMake(coordinatus, spanus)
        mapViewus.region = regionus

        //Put a pin on the address
        let annotation : MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2DMake(58.02474, 14.46796)
        annotation.title = "Timo Jan Brehmer's address"
        mapViewus.addAnnotation(annotation)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
