//
//  AnimationViewController.swift
//  BrokenCV
//
//  Created by DevComputer on 2017-10-28.
//  Copyright © 2017 Timo Brehmer. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animateMyFace()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func animateMyFace() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .repeat, animations: {
            self.imageView.transform = self.imageView.transform.rotated(by: CGFloat(3.14))
        },
        completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
