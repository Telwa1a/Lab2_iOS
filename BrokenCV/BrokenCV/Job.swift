//
//  Job.swift
//  BrokenCV
//
//  Created by DevComputer on 2017-10-29.
//  Copyright © 2017 Timo Brehmer. All rights reserved.
//

import Foundation

struct Job {
    let image: String
    let title : String
    let description : String
}
